import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'login', 
    loadChildren: './login/login.module#LoginPageModule' 
  },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'verification', loadChildren: './verification/verification.module#VerificationPageModule' },
  { path: 'location', loadChildren: './location/location.module#LocationPageModule' },
  { path: 'referralcode', loadChildren: './referralcode/referralcode.module#ReferralcodePageModule' },
  { path: 'whereto-modal', loadChildren: './whereto-modal/whereto-modal.module#WheretoModalPageModule' },
  { path: 'choosecab', loadChildren: './choosecab/choosecab.module#ChoosecabPageModule' },
  { path: 'schedule', loadChildren: './schedule/schedule.module#SchedulePageModule' },
  { path: 'driverinfo', loadChildren: './driverinfo/driverinfo.module#DriverinfoPageModule' },
  { path: 'bookmy-ride', loadChildren: './bookmy-ride/bookmy-ride.module#BookmyRidePageModule' },
  { path: 'my-trips', loadChildren: './my-trips/my-trips.module#MyTripsPageModule' },
  { path: 'help', loadChildren: './help/help.module#HelpPageModule' },
  { path: 'rider-rating', loadChildren: './rider-rating/rider-rating.module#RiderRatingPageModule' },
  { path: 'payment', loadChildren: './payment/payment.module#PaymentPageModule' },
  { path: 'refer-earn', loadChildren: './refer-earn/refer-earn.module#ReferEarnPageModule' },
  { path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' }



  


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
