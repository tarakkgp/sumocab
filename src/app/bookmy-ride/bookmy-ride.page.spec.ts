import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmyRidePage } from './bookmy-ride.page';

describe('BookmyRidePage', () => {
  let component: BookmyRidePage;
  let fixture: ComponentFixture<BookmyRidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookmyRidePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmyRidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
