import { Component, OnInit } from '@angular/core';
import { LoadingController,ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import{ DataService } from '../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookmy-ride',
  templateUrl: './bookmy-ride.page.html',
  styleUrls: ['./bookmy-ride.page.scss'],
})
export class BookmyRidePage implements OnInit {
  viewType:any;
  t_id:any;
  driverinfo:any;
  tripinfo:any;
  fromname:any;
  toname:any;
  drivername:any;
  contactno:any;
  constructor(private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private service: DataService,
    private router: Router) { }


  setViewType(vt) {
    this.viewType = vt;
  }

  ngOnInit() {
    this.service.getnetworkStatus();
    this.storage.get('trip_id').then((trip_id) => {
      this.t_id=trip_id;
      console.log(this.t_id);
      this.getSummeryforTrip();
    });
    
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  async getSummeryforTrip(){
    if(this.service.noNetwork){
      this.presentToast("Network connection error!");
    }else{
      let tripdata={"data":{"trip_id":this.t_id}}
      console.log(JSON.stringify(tripdata));
        const loading = await this.loadingCtrl.create({
          message: ' please wait. . .',
        });
        loading.present();
        this.service.getSummery(tripdata,'get_trip_summery').then((result) => {
          //console.log(result);
          loading.dismiss();
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            this.tripinfo = resArray[2];
            this.fromname = this.tripinfo.from_name;
            this.toname = this.tripinfo.to_name;
            this.driverinfo = resArray[1];
            this.drivername = this.driverinfo.fullName;
            this.contactno = this.driverinfo.user_mobile;
            //this.router.navigateByUrl('/verification')
            //this.router.navigate(['/verification', {otp:userdetails.otp}]);
          }else{
            this.presentToast(resArray[1]);
          }  
        },(err) =>{
          loading.dismiss;
          console.log("eror",JSON.stringify(err));
        }); 
    }  
  }

}
