import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosecabPage } from './choosecab.page';

describe('ChoosecabPage', () => {
  let component: ChoosecabPage;
  let fixture: ComponentFixture<ChoosecabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosecabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosecabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
