import { Component, OnInit } from '@angular/core';
import { ModalController,LoadingController,ToastController } from '@ionic/angular';
import {SchedulePage} from '../schedule/schedule.page';
import{ DataService } from '../service/data.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-choosecab',
  templateUrl: './choosecab.page.html',
  styleUrls: ['./choosecab.page.scss'],
})
export class ChoosecabPage implements OnInit {

  cabs:any;
  cab_id:any;
  
  constructor(
    public modalCtrl: ModalController,
    private service: DataService,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.service.getnetworkStatus();
    this.storage.get('starting_location').then((val) => {
      console.log('starting location is', val);
    });
    this.storage.get('end_location').then((val) => {
      console.log('end location is', val);
    });
    // this.service.getCab().then((res)=>{
    //   this.cabs = res;
    // });
    this.getcablist();
  }
  async getcablist(){
    if(this.service.noNetwork){
      this.presentToast("Network connection error!");
    }else{
      const loading = await this.loadingCtrl.create({
        message: ' please wait. . .',
      });
      loading.present();
      this.service.getvehicle().then((res)=>{
        console.log(res);
        loading.dismiss();
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[0]=="true"){
          this.cabs=resArray[1];
        }
      },(err) =>{
        loading.dismiss;
        console.log("eror",JSON.stringify(err));
      });
    }  
  }

  async gotoSchedule(){
    if(this.cab_id != null){
    const modal = await this.modalCtrl.create({
      component: SchedulePage,
      componentProps:{cabid:this.cab_id}
    });
    return await modal.present();
  }else{
    this.presentToast("Please select any Vehicle.");
  }
  }

  gonextpage(cabid){
    this.cab_id=cabid;
    console.log(this.cab_id);
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

}
