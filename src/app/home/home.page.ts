import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { WheretoModalPage } from '../whereto-modal/whereto-modal.page';
import { IonicModule,MenuController } from '@ionic/angular';
import{ DataService } from '../service/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, AfterContentInit, AfterViewInit,OnDestroy {
  constructor(public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private service: DataService
    ){

  }

  async gotomodal(){
    const modal = await this.modalCtrl.create({
      component: WheretoModalPage
    });
    return await modal.present();

  }

  ngOnInit() {
    console.log(this.service.userid);
    console.log("ngonit");
    // this.menuCtrl.enable(true);
    // this.menuCtrl.swipeEnable(true);
  }
  ngAfterContentInit()  {
    console.log("ngAfterContentInit");
    // this.menuCtrl.enable(true);
    // this.menuCtrl.swipeEnable(true);
  }
  
  ngOnDestroy() {
    console.log(" ngOnDestroy");
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngAfterViewInit() {
    console.log("ngAfterViewInit");
    // this.menuCtrl.enable(true);
    // this.menuCtrl.swipeEnable(true);
  }
  menutoggle(){
    this.menuCtrl.toggle();
  }

}
