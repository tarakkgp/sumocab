import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy} from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IonicModule,MenuController,ToastController,LoadingController } from '@ionic/angular';
import{ DataService } from '../service/data.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, AfterContentInit, AfterViewInit,OnDestroy {

  country:any;
  Phone:any;
  user:any;
  isdisable:boolean;

  constructor(
    public menuCtrl: MenuController,
    private service: DataService,
    private router: Router,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private storage: Storage
  ) {
    this.storage.clear();
  }

  ngOnInit() {
     this.menuCtrl.enable(false);
     this.service.getnetworkStatus();
    // this.menuCtrl.swipeEnable(false);
  }
  ngAfterContentInit()  {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngAfterViewInit() {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngOnDestroy() {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }
  async validateuser(form: NgForm){   
    // console.log(form)
    if (form.invalid) {
      // alert('invalid')
      return;
    }
    if(this.service.noNetwork){
      this.presentToast("Network connection error!")
    }else{
        let logindata={"data":{"mobile_no":this.Phone}}
        console.log(JSON.stringify(logindata));
        const loading = await this.loadingCtrl.create({
          message: ' please wait. . .',
        });
        loading.present();
        this.service.login(logindata,'login').then((result) => {
          console.log(result);
          loading.dismiss();
          const resArray = Object.keys(result).map(i => result[i]);
          if(resArray[0]=="true"){
            let userdetails=resArray[2];
            this.storage.set('user_id',userdetails._id).then((user_id)=>{
            });
            //this.storage.set(STORAGE_KEY,userdetails._id);
            this.service.loggedInPhone=userdetails.user_mobile;
            this.service.userid=userdetails._id;
            this.service.loggedInUsername=userdetails.username;
            this.service.usertype=userdetails.user_type;
            console.log(this.service.loggedInPhone);
            console.log(this.service.userid);
            console.log(this.service.loggedInUsername);
            console.log(this.service.usertype);
            // this.storage.get('user_id').then((user_id) => {
            //   console.log('userid is', user_id);
            // });
            //this.router.navigateByUrl('/verification');
            this.router.navigate(['/verification', {otp:userdetails.otp}]);
          }else{
            this.presentToast(resArray[1]);
          }  
        },(err) =>{
          loading.dismiss;
          console.log("sign up eror",JSON.stringify(err));
        }); 
    }    
  }

  async validnumber(event){
    console.log(event.target.value.length);
    if(event.target.value.length != 10){
      if(event.target.value.length>10){
        const toast =  await this.toastCtrl.create({
          message:"Please enter 10 digit Phone no.",
          duration: 1000
        });
        toast.present();
      }

      this.isdisable=true;
    }else{
      this.isdisable=false;
    }
  }

}
