import { Component, OnInit } from '@angular/core';
import { LoadingController,ToastController} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import{ DataService } from '../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-trips',
  templateUrl: './my-trips.page.html',
  styleUrls: ['./my-trips.page.scss'],
})
export class MyTripsPage implements OnInit {
  tab:any="past";
  user_id:any;
  trips:any;
  presentTrip:any;
  constructor(private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private service: DataService,
    private router: Router) { }

  ngOnInit() {
    this.service.getnetworkStatus();
    this.storage.get('user_id').then((user_id) => {
      this.user_id=user_id;
      console.log(this.user_id);
      this.getTrip();
    });
    
  }

  async getTrip(){
    if(this.service.noNetwork){
      this.presentToast("Network connection error!");
    }else{
      let feeddata={"data":{"user_id":this.user_id}}
      console.log(JSON.stringify(feeddata));
        const loading = await this.loadingCtrl.create({
          message: ' please wait. . .',
        });
        loading.present();
        this.service.myTrip(feeddata,'customer_all_trip').then((result) => {
          console.log(result);
          loading.dismiss();
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            this.trips=resArray[1];
            this.presentTrip=resArray[2];
          }  
        },(err) =>{
          loading.dismiss;
          console.log("eror",JSON.stringify(err));
        });   
    }
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

}
