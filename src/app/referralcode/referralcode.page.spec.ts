import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralcodePage } from './referralcode.page';

describe('ReferralcodePage', () => {
  let component: ReferralcodePage;
  let fixture: ComponentFixture<ReferralcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralcodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
