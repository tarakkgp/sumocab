import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiderRatingPage } from './rider-rating.page';

describe('RiderRatingPage', () => {
  let component: RiderRatingPage;
  let fixture: ComponentFixture<RiderRatingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiderRatingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiderRatingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
