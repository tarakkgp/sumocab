import { Component, OnInit } from '@angular/core';
import { LoadingController,ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import{ DataService } from '../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rider-rating',
  templateUrl: './rider-rating.page.html',
  styleUrls: ['./rider-rating.page.scss'],
})
export class RiderRatingPage implements OnInit {
  rate:any;
  t_id:any;
  user_id:any;
  constructor( private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private service: DataService,
    private router: Router) { }

  ngOnInit() {
    this.service.getnetworkStatus();
    this.storage.get('user_id').then((user_id) => {
      this.user_id=user_id;
    });
    this.storage.get('trip_id').then((trip_id) => {
      this.t_id=trip_id;
      console.log(this.t_id);
    });
  }

  onRateChange(event) {
    console.log('Your rate:', event);
    this.rate=event;
  }

  async setFeedback(){
    if(this.service.noNetwork){
      this.presentToast("Network connection error!");
    }else{
      if(this.rate!=null){
      let feeddata={"data":{"trip_id":this.t_id,"customer_id":this.user_id,"rating":this.rate}}
      console.log(JSON.stringify(feeddata));
        const loading = await this.loadingCtrl.create({
          message: ' please wait. . .',
        });
        loading.present();
        this.service.feedback(feeddata,'submit_feedback').then((result) => {
          //console.log(result);
          loading.dismiss();
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            this.presentToast(resArray[1]);
            this.router.navigateByUrl('/home');
          }else{
            this.presentToast(resArray[1]);
          }  
        },(err) =>{
          loading.dismiss;
          console.log("eror",JSON.stringify(err));
        }); 
      }else{
        this.presentToast("Please submit your feedback.");
      }  
    }  
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

}
