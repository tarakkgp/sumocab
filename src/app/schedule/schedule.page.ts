import { Component, OnInit } from '@angular/core';
import { ModalController,NavParams,LoadingController,ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import{ DataService } from '../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {
  startingpoint:any;
  endpoint:any;
  user_id:any;
  vehicle_id:any;
  date_time:any;
  startlat:any;
  startlon:any;
  endlat:any;
  endlon:any;
  today:any;

  constructor(public modal:ModalController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private service: DataService,
    private router: Router) {
    this.vehicle_id=navParams.get("cabid");
    console.log(this.vehicle_id);
    let d = new Date().toISOString();
    console.log(d);
    let v = d.split('T');
    this.today = v[0];
    console.log(this.today);
   }

  ngOnInit() {
    this.service.getnetworkStatus();
    this.storage.get('user_id').then((user_id) => {
      this.user_id=user_id;
    });
    this.storage.get('starting_location').then((starting_location) => {
      this.startingpoint=starting_location;
      console.log(this.startingpoint);
      this.service.getlattudelongitude(this.startingpoint).then((result) => {
        console.log(result);
        const resArray = Object.keys(result).map(i => result[i]);
        if(resArray[1]=="OK"){
          console.log(resArray[0]);
          const local= resArray[0];
          const resArrayresult = Object.keys(local).map(i => local[i]);
          this.startlat =resArrayresult[0].geometry.location.lat;
          this.startlon =resArrayresult[0].geometry.location.lng;
          console.log(this.startlat);
          console.log(this.startlon);
        }   
      }, (err) =>{
        console.log("sign up eror",JSON.stringify(err));
      }); 
    });

    this.storage.get('end_location').then((end_location) => {
      this.endpoint=end_location;
      console.log(this.startingpoint);
      console.log(this.endpoint);
      //this.service.getdrivingDistance(this.startingpoint,this.endpoint);
      // this.service.getlattudelongitude(this.endpoint).then((result) => {
      //   console.log(result);
      //   const resArray = Object.keys(result).map(i => result[i]);
      //   if(resArray[1]=="OK"){
      //     console.log(resArray[0]);
      //     const local= resArray[0];
      //     const resArrayresult = Object.keys(local).map(i => local[i]);
      //     this.endlat =resArrayresult[0].geometry.location.lat;
      //     this.endlon =resArrayresult[0].geometry.location.lng;
      //     console.log(this.endlat);
      //     console.log(this.endlon);
      //     this.getDistanceFromLatLonInKm(this.startlat,this.startlon,this.endlat,this.endlon);
      //   }   
      // }, (err) =>{
      //   console.log("sign up eror",JSON.stringify(err));
      // });
    });
    
  }

  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    console.log(d)
  }
  
  deg2rad(deg) {
    return deg * (Math.PI/180)
  }


  dissmiss(){
    this.modal.dismiss();
  }

  async setTrip(){
    if(this.service.noNetwork){
      this.presentToast("Network connection error!");
    }else{
      if(this.date_time != null){
        let userData = {
          "data": {
            "user_id": this.user_id,
            "starting_point": this.startingpoint,
            "end_point": this.endpoint,
            "vehicle_id": this.vehicle_id,
            "start_date": this.date_time,
            "trip_type": "one"
          }
        }
        console.log(JSON.stringify(userData));
        const loading = await this.loadingCtrl.create({
          message: ' please wait. . .',
        });
        loading.present();
        this.service.addtrip(userData,'add_trip').then((result) => {
          console.log(result);
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          loading.dismiss();
          if(resArray[0]=="true"){
            this.presentToast(resArray[1]);
            console.log(resArray[2]._id);
            this.storage.set('trip_id',resArray[2]._id).then((trip_id)=>{
            });
            this.router.navigateByUrl('/driverinfo');
          }
        }, (err) =>{
          loading.dismiss;
          console.log("sign up eror",JSON.stringify(err));
        }); 
        this.dissmiss(); 
      }else{
        this.presentToast("Please select the date and time.");
      }
    } 
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

}
