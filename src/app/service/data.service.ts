import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ArrayType } from '@angular/compiler';


let apiUrl = "http://192.168.0.124:3000/api/";


@Injectable({
  providedIn: 'root'
})
export class DataService {
  loggedInUsername:any;
  loggedInPhone:any;
  theCustomerId:any;
  otp:any;
  usertype:any;
  userid:any;
  noNetwork:boolean;

  user = "http://localhost:3000/users";
  cablist = "../assets/cablist.json";

  constructor(public http: HttpClient ) {
    
  }

  getUser(){
    return new Promise((resolve, reject) => {
      this.http.get(this.user).subscribe(res => {
      resolve(res);
      //console.log(res);
      }, (err) => {
      reject(err);
      console.log(err);
      });
    });  }

  getCab(){
    return new Promise(resolve => {
      this.http.get(this.cablist).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  newRegister(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  login(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  otpverfiyed(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  getvehicle(){
    return new Promise(resolve => {
      this.http.get(apiUrl+'all_vehicle').subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  addtrip(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  getlattudelongitude(location){
    return new Promise(resolve => {
      let url = "https://maps.googleapis.com/maps/api/geocode/json?address=1600+"+location+"&key=AIzaSyAgXLi8S6pYfoNfCCBAmDW5PUh4yTPPKYM";
      console.log(url)
      this.http.get(url).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getdrivingDistance(start,end){
    return new Promise(resolve => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Content-Type', 'application/json');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Access-Control-Allow-Headers','application/json');
      //let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+start+"&destinations="+end+"&mode=driving&key=AIzaSyCZr5giYgXzXjvOSYfA_LndEkBg6LVuQIc";
      let url ="https://maps.googleapis.com/maps/api/distancematrix/json?origins=Midnapore, West Bengal, India&destinations=Balichak, West Bengal, India&mode=driving&key=AIzaSyCZr5giYgXzXjvOSYfA_LndEkBg6LVuQIc"
      console.log(url)
      this.http.get(url,{ headers: headers }).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getSummery(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  feedback(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  myTrip(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  getnetworkStatus(){
    if(!navigator.onLine){
      this.noNetwork = true;
    }else{
      this.noNetwork = false;
    }

     window.addEventListener('offline', () => {
      this.noNetwork = true;
      });
      window.addEventListener('online', () => {
        this.noNetwork = false;
      });
  }
  
}
