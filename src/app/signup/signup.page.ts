import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy } from '@angular/core';
import { IonicModule,MenuController,ToastController,LoadingController } from '@ionic/angular';
import{ DataService } from '../service/data.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit, AfterContentInit, AfterViewInit,OnDestroy {
  uname:any;
  uphone:any;
  isdisable:boolean;

  constructor(public menuCtrl: MenuController,
    private service: DataService,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private router: Router,
    private storage: Storage
  ) { }

  ngOnInit() {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngAfterContentInit()  {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngAfterViewInit() {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngOnDestroy() {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

   async signupUser(form: NgForm){   
    console.log(form)
    if (form.invalid) {
      // alert('invalid')
      return;
    }else{
      let userData = {
        "data": {
          "mobile_no": this.uphone,
          "name":this.uname,
          "user_type":"customer"
        }
      }
      console.log(JSON.stringify(userData));
      const loading = await this.loadingCtrl.create({
        message: ' please wait. . .',
      });
      loading.present();
      this.service.newRegister(userData,'registration').then((result) => {
        //console.log(result);
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        loading.dismiss();
        if(resArray[0]=="true"){
          this.presentToast(resArray[1]);
          let userdetails=resArray[2];
          this.storage.set('user_id',userdetails._id).then((user_id)=>{
          });
          this.service.loggedInPhone=userdetails.user_mobile;
          this.service.userid=userdetails._id;
          this.service.loggedInUsername=userdetails.username;
          this.service.usertype=userdetails.user_type;
          console.log(this.service.loggedInPhone);
          console.log(this.service.userid);
          console.log(this.service.loggedInUsername);
          console.log(this.service.usertype);
          //this.router.navigateByUrl('/verification');
          this.router.navigate(['/verification', {otp:userdetails.otp}]);
        }else{
          this.presentToast(resArray[1]);
        }  
      }, (err) =>{
        loading.dismiss;
        console.log("sign up eror",JSON.stringify(err));
      });   
    }
    // this.service.saveUser(this.users).then((result) => {
    //   console.log(result); 
    // }, (err) => { 
    //   console.log(err);
    // }); 
  }

  async validnumber(event){
    console.log(event.target.value.length);
    if(event.target.value.length != 10){
      if(event.target.value.length>10){
        const toast =  await this.toastCtrl.create({
          message:"Please enter 10 digit Phone no.",
          duration: 1000
        });
        toast.present();
      }

      this.isdisable=true;
    }else{
      this.isdisable=false;
    }
  }

}
