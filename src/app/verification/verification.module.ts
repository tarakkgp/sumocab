import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule,MenuController } from '@ionic/angular';

import { VerificationPage } from './verification.page';

const routes: Routes = [
  {
    path: '',
    component: VerificationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VerificationPage]
})
export class VerificationPageModule {
  constructor(public menuCtrl:MenuController){
  }
  // ionViewWillEnter() {
  //   this.menuCtrl.enable(false);
  // }

}
