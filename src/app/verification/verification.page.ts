import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy} from '@angular/core';
import { IonicModule,MenuController,LoadingController,ToastController } from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import{ DataService } from '../service/data.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-verification',
  templateUrl: './verification.page.html',
  styleUrls: ['./verification.page.scss'],
})
export class VerificationPage implements OnInit, AfterContentInit, AfterViewInit,OnDestroy {
  userotp:any;
  user_id:any;

  constructor(public menuCtrl: MenuController,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private service: DataService,
    private router: Router,
    private storage: Storage
    ) {
    this.userotp = this.route.snapshot.paramMap.get('otp');
    console.log(this.userotp);
   }

  ngOnInit() {
     this.menuCtrl.enable(false);
     this.service.getnetworkStatus();
    // this.menuCtrl.swipeEnable(false);
    this.storage.get('user_id').then((user_id) => {
      this.user_id=user_id;
    });
  }
  ngAfterContentInit()  {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngAfterViewInit() {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  ngOnDestroy() {
    // this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }

  async otpverification(){
    if(this.service.noNetwork){
      this.presentToast("Network connection error!");
    }else{
      let userData = {
        "data": {
          "otp": this.userotp,
          "user_id":this.user_id
        }
      }
      console.log(JSON.stringify(userData));
      const loading = await this.loadingCtrl.create({
        message: ' please wait. . .',
      });
      loading.present();
      this.service.otpverfiyed(userData,'otp_verification').then((result) => {
        console.log(result);
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        loading.dismiss();
        if(resArray[0]=="true"){
          this.presentToast(resArray[1]);
          this.router.navigateByUrl('/home');
        }
      }, (err) =>{
        loading.dismiss;
        console.log("sign up eror",JSON.stringify(err));
      }); 
    }  
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  async resendotpverification(){
    if(this.service.noNetwork){
      this.presentToast("Network connection error!");
    }else{
      let userData = {
        "data": {
          "user_id":this.user_id
        }
      }
      console.log(JSON.stringify(userData));
      const loading = await this.loadingCtrl.create({
        message: ' please wait. . .',
      });
      loading.present();
      this.service.otpverfiyed(userData,'resend_otp').then((result) => {
        console.log(result);
        const resArray = Object.keys(result).map(i => result[i]);
        loading.dismiss();
        if(resArray[0]=="true"){
        this.userotp=resArray[1];
        }
      }, (err) =>{
        loading.dismiss;
        console.log("sign up eror",JSON.stringify(err));
      }); 
    }  
  }
}
