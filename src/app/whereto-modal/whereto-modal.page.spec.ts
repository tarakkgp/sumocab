import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WheretoModalPage } from './whereto-modal.page';

describe('WheretoModalPage', () => {
  let component: WheretoModalPage;
  let fixture: ComponentFixture<WheretoModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WheretoModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WheretoModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
