import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { ModalController} from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';



declare var google:any;

@Component({
  selector: 'app-whereto-modal',
  templateUrl: './whereto-modal.page.html',
  styleUrls: ['./whereto-modal.page.scss'],
})
export class WheretoModalPage implements OnInit {
  autocompleteItems: any;
  autocomplete = {
    query: ''
    };
  input:any;
  //tolocation...
  acService:any;
  autocompleteEndItems: any;
  autocompleteN = {
    nquery: ''
    };

  constructor(
    public modalCtrl: ModalController,
    public ref:ChangeDetectorRef,
    private router: Router,
    private storage: Storage) 
    { 

    }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };

    this.autocompleteEndItems = [];
    this.autocompleteN = {
      nquery: ''
    };
  }

  closeModal()
  {
    this.modalCtrl.dismiss();
    this.router.navigateByUrl('/choosecab');
  }

  updateSearch() {
    console.log(this.autocomplete.query);
    
    if (this.autocomplete.query == '') {
    this.autocompleteItems = [];
    return;
    }
    let self = this; 
    let config = { 
    
    input: this.autocomplete.query, 
    componentRestrictions: {  } 
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
    
    self.autocompleteItems = [];
    if(predictions!== null){
      predictions.forEach(function (prediction) {              
        self.autocompleteItems.push(prediction);
        self.ref.detectChanges();
        });
    }        
    });
  }
  updateendSearch(){
    console.log(this.autocompleteN.nquery);
    
    if (this.autocompleteN.nquery == '') {
    this.autocompleteEndItems = [];
    return;
    }
    let self = this; 
    let config = { 
    
    input: this.autocompleteN.nquery, 
    componentRestrictions: {  } 
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
    
    self.autocompleteEndItems = [];
    if(predictions!== null){
      predictions.forEach(function (prediction) {              
        self.autocompleteEndItems.push(prediction);
        self.ref.detectChanges();
        });
    }        
    });
  }

  getLocation(pos){
    this.autocomplete.query=pos;
    this.storage.set('starting_location',pos).then((starting_location)=>{
    });
    console.log(pos);
    this.autocompleteItems = [];
  }

  getendLocation(pos){
    this.autocompleteN.nquery=pos;
    this.storage.set('end_location',pos).then((end_location)=>{
    });
    console.log(pos);
    this.autocompleteEndItems = [];
  }


}
